<!DOCTYPE html>
<html>
<head>
      <meta charset='utf-8' />

      <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
      <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
      <link rel="stylesheet" href="./datatable.css" />
      <title>Etat Machines</title>
      <div id="en_tete">
      <p>  Voici l'équipement à disposition ! <img src = "./icam.png" class = "flotte" id = "logo">
	  <h2><a href="identification.php" onclick="signOut();">Déconnexion</a></h2>
	  <h2><a href="equipement.php" >Acceder à l'agenda</a></h2>
	  </p>

      </div>
</head>
<body>
  <?php
  $db = new mysqli('mysql-aerath.alwaysdata.net','aerath_iot','mdp_iot','aerath_iot2');
  if ($db->connect_error)
    die("Connection echoué à la base de données:{$db -> connect_error}");
  $res1 = $db->query('SELECT * FROM Equipement ORDER BY nom');
  ?>
<table id="myTable" class="display">
    <thead>
        <tr>
            <th>Equipement</th>
            <th>Code ICAM</th>
            <th>Localisation</th>
			      <th>Disponibilité</th>
            <th>Constructeur</th>
            <th>Année d'achat</th>
            <th>Amortissement</th>
            <th>Coût</th>
            <th>Coût de Maintenance</th>
        </tr>
    </thead>
    <tbody>
      <?php
      while ($row1 = $res1->fetch_assoc()){
      ?>
      <tr>

        <td><?php echo $row1['nom']; ?></td>
        <td><?php echo $row1['code_icam']; ?></td>
        <td><?php echo $row1['localisation']; ?></td>
        <td><?php echo $row1['disponibilite']; ?></td>
        <td><?php echo $row1['constructeur']; ?></td>
        <td><?php echo $row1['annee_achat']; ?></td>
        <td><?php echo $row1['amortissement']; ?></td>
        <td><?php echo $row1['cout']; echo(' €'); ?></td>
        <td><?php echo $row1['cout_maintenance']; echo(' €'); ?></td>
      </tr>
      <?php
      }
      ?>
    </tbody>
</table>
///////////AJOUT D'EQUIPEMENT/////////////
  <form action="ajout_machine.php" method="post" class="champ_form">
    <B>Ajouter une machine :</B>
    Nom : <input type="texte" name="nom" size="10">
    Code Icam : <input type="texte" name="code_icam" size="5">
    Localisation : <input type="texte" name="localisation" size="10">
    Disponibilité : <select name="disponibilite">
          <option value="OUI">OUI</option>
          <option value="NON">NON</option>
          <option value="HS">HS</option> </select>
    Constructeur : <input type="texte" name="constructeur" size="10" >
    Année d'achat : <?php
            $selected = '';
            echo '<select name="annee_achat">',"\n";
            for($i=1980; $i<=2030; $i++)
            {
              if($i == date('Y'))
              {
                $selected = ' selected="selected"';
              }
              echo "\t",'<option value="', $i ,'"', $selected ,'>', $i ,'</option>',"\n";
              $selected='';
            }
            echo '</select>',"\n";
            ?>
    Amortissement  <input type="texte" name="amortissement" size="5">
    Coût :  <input type="texte" name="cout"size="5"> €
    Coût de Maintenance :  <input type="texte" name="cout_maintenance"size="5"> €
    <input type="submit" value="Ajouter l'équipement">
  </form>

  ///////////MODIFICATION D'EQUIPEMENT/////////////
  <form action="modification_machine.php" method="post" class="champ_form">
    <B>Modifier une machine :</B>
    Code Icam : <input type="texte" name="code_icam" size="5">
    Disponibilité : <select name="disponibilite">
          <option value="OUI">OUI</option>
          <option value="NON">NON</option>
          <option value="HS">HS</option> </select>
    Constructeur : <input type="texte" name="constructeur" size="10" >
    Année d'achat : <?php
            $selected = '';
            echo '<select name="annee_achat">',"\n";
            for($i=1980; $i<=2030; $i++)
            {
              if($i == date('Y'))
              {
                $selected = ' selected="selected"';
              }
              echo "\t",'<option value="', $i ,'"', $selected ,'>', $i ,'</option>',"\n";
              $selected='';
            }
            echo '</select>',"\n";
            ?>
    Amortissement  <input type="texte" name="amortissement" size="5">
    Coût :  <input type="texte" name="cout"size="5"> €
    Coût de Maintenance :  <input type="texte" name="cout_maintenance"size="5"> €
    <input type="submit" value="Modifier l'équipement">
    <script>
    $.post("ajout_machine.php", {'lmachine':equipement, 'ini':arg.startStr, 'fin':arg.endStr, 'llieu':'unknown'});
    </script>
  </form>
  <!-- <script language = "javascript" src ="./datatable.js"> </script> -->
</body>
<script>
$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>

 <script>
		function signOut() {
		var auth2 = gapi.auth2.getAuthInstance();
		auth2.signOut().then(function () {
		console.log('User signed out.');
		});
		}
	  </script>

</html>
