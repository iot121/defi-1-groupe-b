
/*
 * Editor client script for DB table Equipement
 * Created by http://editor.datatables.net/generator
 */
 
 var equipement_selected = "";
 
function infos(){
	if(equipement_selected != ""){
		window.location.href = './selectable_perso.php?nom='+equipement_selected;
	}
}

(function($){

$(document).ready(function() {
	var editor = new $.fn.dataTable.Editor( {
		ajax: 'php/table.Equipement.php',
		table: '#Equipement',
		fields: [
			{
				"label": "Equipement:",
				"name": "nom"
			},
			{
				"label": "Code ICAM:",
				"name": "code_icam"
			},
			{
				"label": "Localisation:",
				"name": "localisation"
			},
			{
				"label": "Disponibilite:",
				"name": "disponibilite"
			},
			{
				"label": "Constructeur:",
				"name": "constructeur"
			},
			{
				"label": "NB dossier:",
				"name": "nb_dossier"
			},
			{
				"label": "Etat:",
				"name": "etat"
			},
			{
				"label": "Annee achat:",
				"name": "annee_achat"
			},
			{
				"label": "Amortissement:",
				"name": "amortissement"
			},
			{
				"label": "Cout:",
				"name": "cout"
			},
			{
				"label": "Cout maintenance:",
				"name": "cout_maintenance"
			}
		]
	} );

	var table = $('#Equipement').DataTable( {
		ajax: 'php/table.Equipement.php',
		columns: [
			{
				"data": "nom"
			},
			{
				"data": "code_icam"
			},
			{
				"data": "localisation"
			},
			{
				"data": "disponibilite"
			},
			{
				"data": "constructeur"
			},
			{
				"data": "annee_achat"
			},
			{
				"data": "amortissement"
			},
			{
				"data": "cout"
			},
			{
				"data": "cout_maintenance"
			}
		],
		select: true,
		lengthChange: false
	} );

	new $.fn.dataTable.Buttons( table, [
		{ extend: "create", editor: editor },
		{ extend: "edit",   editor: editor },
		{ extend: "remove", editor: editor }
	] );

	table.buttons().container()
		.appendTo( $('.col-md-6:eq(0)', table.table().container() ) );
	
	var table = $('#Equipement').DataTable();
	table.on( 'select', function () {
		var rowData = table.rows( { selected: true } ).data()[0];
		equipement_selected = rowData.nom;
		console.log(equipement_selected);
	} );
	
} );

}(jQuery));

