<!DOCTYPE html>
<?php $nom = $_GET['nom']; ?>
<html>
<head>
<meta charset='utf-8' />

<title>Reservation - Calendrier</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/main.min.css" integrity="sha256-Lfe6+s5LEek8iiZ31nXhcSez0nmOxP+3ssquHMR3Alo=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/daygrid/main.min.css" integrity="sha256-AVsv7CEpB2Y1F7ZjQf0WI8SaEDCycSk4vnDRt0L2MNQ=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/timegrid/main.min.css" integrity="sha256-DOWdbe6a1VwJwFpkimY6z5tW9mmrBNre2jZsAige5PE=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/list/main.min.css" integrity="sha256-saO3mkZVAcyqsfgsGMrmE7Cs/TLN1RgVykZ5dnnJKug=" crossorigin="anonymous" />
<style>
.fc-event-container {
	color: white;
	cursor: pointer;
}
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/main.min.js" integrity="sha256-GBryZPfVv8G3K1Lu2QwcqQXAO4Szv4xlY4B/ftvyoMI=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/interaction/main.min.js" integrity="sha256-MUHmW5oHmLLsvmMWBO8gVtKYrjVwUSFau6pRXu8dtnA=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/daygrid/main.min.js" integrity="sha256-FT1eN+60LmWX0J8P25UuTjEEE0ZYvpC07nnU6oFKFuI=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/timegrid/main.min.js" integrity="sha256-L9T+qE3Ms6Rsuxl+KwLST6a3R/2o6m33zB5mR2KyPjU=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/locales-all.min.js" integrity="sha256-W3h/kWvQYYadUwb7/Tcf5WkDq3q0VPleove6MyKZS8o=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/list/main.min.js" integrity="sha256-q57s73NpMCTQ4ZXqb1X5bIywrICySeB6WvYxFGfz/PA=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<script>
"use strict";
	function createEvent(arg){
		calendar.addEvent({
			title: equipement,
			start: arg.start,
			end: arg.end,
			allDay: arg.allDay
		});
		$.post("envoi.php", {'lmachine':equipement, 'ini':arg.startStr, 'fin':arg.endStr, 'llieu':'unknown'});
		console.log("Reservation envoyée");
		$('#createEventModal').modal('hide');
	}
	
	function delEvent(event){
		event.remove();
		$.post("suppression.php", {'id':event.extendedProps.db_id});
		console.log("Supprimée");
		$('#delEventModal').modal('hide');
	}
	
	let calendar;
	var equipement = "<?php echo $nom; ?>";
	
  document.addEventListener('DOMContentLoaded', function() {
    let calendarEl = document.getElementById('calendar');

    calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay listDay,listWeek'
      },
	  views: {
        listDay: { buttonText: 'Liste par jour' },
        listWeek: { buttonText: 'Liste par semaine' }
      },
	  locale: 'fr',
      navLinks: true, // can click day/week names to navigate views
      selectable: <?php echo ($nom=="*" ? "false":"true") ?>,
	  selectOverlap: false,
      selectMirror: true,
      select: function(arg) {
		$("#createEventModal .modal-body").html("<b>Equipement:</b> "+equipement+"<br><b>Debut:</b> "+arg.start.toLocaleString('fr-fr', { timeZone: 'Europe/Paris' })+"<br><b>Fin:</b>&nbsp&nbsp&nbsp&nbsp&nbsp "+arg.end.toLocaleString('fr-fr', { timeZone: 'Europe/Paris' }));
		$('#createEventModal').modal();
		$('#createEventBtn').off("click.createEvent").on("click.createEvent", () => createEvent(arg));
		
        calendar.unselect()
      },
	  eventClick: function(calEvent, jsEvent, view) {
		  console.log("click");
		  let event = calEvent.event;
		  
		  $('#delEventModal .modal-body').html("<b>Equipement:</b> "+event.title+"<br><b>Debut:</b> "+event.start.toLocaleString('fr-fr', { timeZone: 'Europe/Paris' })+"<br><b>Fin:</b>&nbsp&nbsp&nbsp&nbsp&nbsp "+event.end.toLocaleString('fr-fr', { timeZone: 'Europe/Paris' }));
		  $('#delEventModal').modal();
		  $('#delEventBtn').off("click.delEvent").on("click.delEvent", () => delEvent(event));

	  },
      editable: false,
      eventLimit: true, // allow "more" link when too many events
      events: 
<?php
	$db = new mysqli('mysql-aerath.alwaysdata.net','aerath_iot','mdp_iot','aerath_iot2');
	if ($db->connect_error)
		die("Connection echoué à la base de données:{$db -> connect_error}");
	if($nom=="*"){
		$res1 = $db->query('select id AS db_id, nom AS title, DATE_FORMAT(date_ini, "%Y-%m-%dT%T") AS start, DATE_FORMAT(date_fin, "%Y-%m-%dT%T") AS end from Reservation');
	}else{
		$res1 = $db->query('select id AS db_id, nom AS title, DATE_FORMAT(date_ini, "%Y-%m-%dT%T") AS start, DATE_FORMAT(date_fin, "%Y-%m-%dT%T") AS end from Reservation WHERE nom="'.$nom.'"');
	}
	
	while ($row1 = $res1->fetch_assoc()){
		$liste[] = $row1;
	}
		print json_encode($liste);
?>
      
    });

    calendar.render();
  });
</script>
<style>

  body {
    margin: 40px 10px;
    padding: 0;
    font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
    font-size: 14px;
  }

  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }

</style>
</head>
<body>

<div class="modal fade" id="createEventModal" tabindex="-1" role="dialog" aria-labelledby="createEventModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Voulez-vous créer la réservation suivante :</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" id="createEventBtn" class="btn btn-primary">Confirmer</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="delEventModal" tabindex="-1" role="dialog" aria-labelledby="delEventModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Voulez-vous supprimer la réservation suivante :</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" id="delEventBtn" class="btn btn-primary">Confirmer</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
      </div>
    </div>
  </div>
</div>

<div style="text-align: center;margin-bottom:20px;">
<h1>Calendrier des réservations</h1>
</div>

  <div id='calendar'></div>

  <div style="text-align: center;margin-top:20px;">
  <button type="button" class="btn btn-secondary" onclick="window.location.href='./Equipement.html'">Retour</button>
  </div>

</body>
</html>
