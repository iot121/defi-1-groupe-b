
/*
 * Editor client script for DB table Equipement
 * Created by http://editor.datatables.net/generator
 */

var equipement_selected = "";
 
function infos(){
	if(equipement_selected != ""){
		window.location.href = './selectable_perso.php?nom='+equipement_selected;
	}
}

(function($){

$(document).ready(function() {
	var editor = new $.fn.dataTable.Editor( {
		ajax: 'php/table.Equipement.php',
		table: '#Equipement',
		fields: [
			{
				"label": "Equipement:",
				"name": "nom"
			},
			{
				"label": "Code Icam:",
				"name": "code_icam"
			},
			{
				"label": "Localisation:",
				"name": "localisation"
			},
			{
				"label": "Disponibilit&eacute;:",
				"name": "disponibilite",
				"type": "select",
				"def": "OUI",
				"options": [
					"OUI",
					"NON",
					"HS"
				]
			},
			{
				"label": "Constructeur:",
				"name": "constructeur"
			},
			{
				"label": "Nombre de Dossier:",
				"name": "nb_dossier"
			},
			{
				"label": "Ann&eacute;e d'achat:",
				"name": "annee_achat",
				"type": "select",
				"def": "2020",
				"options": [
					"1980",
					"1981",
					"1982",
					"1983",
					"1984",
					"1985",
					"1986",
					"1987",
					"1988",
					"1989",
					"1990",
					"1991",
					"1992",
					"1993",
					"1994",
					"1995",
					"1996",
					"1997",
					"1998",
					"1999",
					"2000",
					"2001",
					"2002",
					"2003",
					"2004",
					"2005",
					"2006",
					"2007",
					"2008",
					"2009",
					"2010",
					"2011",
					"2012",
					"2013",
					"2014",
					"2015",
					"2016",
					"2017",
					"2018",
					"2019",
					"2020",
					"2021",
					"2022",
					"2023",
					"2024",
					"2025",
					"2026",
					"2027",
					"2028",
					"2029",
					"2030",
					"2031",
					"2032",
					"2033",
					"2034",
					"2035",
					"2036",
					"2037",
					"2038",
					"2039"
				]
			},
			{
				"label": "Amortissement:",
				"name": "amortissement"
			},
			{
				"label": "Co&ucirc;t:",
				"name": "cout"
			},
			{
				"label": "Co&ucirc;t de Maintenance:",
				"name": "cout_maintenance"
			}
		]
	} );

	var table = $('#Equipement').DataTable( {
		ajax: 'php/table.Equipement.php',
		columns: [
			{
				"data": "nom"
			},
			{
				"data": "code_icam"
			},
			{
				"data": "localisation"
			},
			{
				"data": "disponibilite"
			},
			{
				"data": "constructeur"
			},
			{
				"data": "nb_dossier"
			},
			{
				"data": "annee_achat"
			},
			{
				"data": "amortissement"
			},
			{
				"data": "cout"
			},
			{
				"data": "cout_maintenance"
			}
		],
		select: true,
		lengthChange: false
	} );

	new $.fn.dataTable.Buttons( table, [
		{ extend: "create", editor: editor },
		{ extend: "edit",   editor: editor },
		{ extend: "remove", editor: editor }
	] );

	table.buttons().container()
		.appendTo( $('.col-md-6:eq(0)', table.table().container() ) );
		
	var table = $('#Equipement').DataTable();
	table.on( 'select', function () {
		var rowData = table.rows( { selected: true } ).data()[0];
		equipement_selected = rowData.nom;
		console.log(equipement_selected);
	} );
} );

}(jQuery));

