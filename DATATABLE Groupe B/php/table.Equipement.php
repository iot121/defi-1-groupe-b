<?php

/*
 * Editor server script for DB table Equipement
 * Created by http://editor.datatables.net/generator
 */

// DataTables PHP library and database connection
include( "lib/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate,
	DataTables\Editor\ValidateOptions;

// The following statement can be removed after the first run (i.e. the database
// table has been created). It is a good idea to do this to help improve
// performance.
$db->sql( "CREATE TABLE IF NOT EXISTS `Equipement` (
	`id` int(10) NOT NULL auto_increment,
	`nom` varchar(255),
	`code_icam` varchar(255),
	`localisation` varchar(255),
	`disponibilite` varchar(255),
	`constructeur` varchar(255),
	`nb_dossier` numeric(9,2),
	`annee_achat` varchar(255),
	`amortissement` numeric(9,2),
	`cout` numeric(9,2),
	`cout_maintenance` numeric(9,2),
	PRIMARY KEY( `id` )
);" );

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'Equipement', 'id' )
	->fields(
		Field::inst( 'nom' )
			->validator( Validate::notEmpty() ),
		Field::inst( 'code_icam' )
			->validator( Validate::notEmpty() ),
		Field::inst( 'localisation' ),
		Field::inst( 'disponibilite' ),
		Field::inst( 'constructeur' ),
		Field::inst( 'nb_dossier' ),
		Field::inst( 'annee_achat' ),
		Field::inst( 'amortissement' ),
		Field::inst( 'cout' ),
		Field::inst( 'cout_maintenance' )
	)
	->process( $_POST )
	->json();
