-- 
-- Editor SQL for DB table Equipement
-- Created by http://editor.datatables.net/generator
-- 

CREATE TABLE IF NOT EXISTS `Equipement` (
	`id` int(10) NOT NULL auto_increment,
	`nom` varchar(255),
	`code_icam` varchar(255),
	`localisation` varchar(255),
	`disponibilite` varchar(255),
	`constructeur` varchar(255),
	`nb_dossier` numeric(9,2),
	`annee_achat` varchar(255),
	`amortissement` numeric(9,2),
	`cout` numeric(9,2),
	`cout_maintenance` numeric(9,2),
	PRIMARY KEY( `id` )
);